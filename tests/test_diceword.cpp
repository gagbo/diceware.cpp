#include "catch2/catch.hpp"
#include "diceware/diceword.hpp"

TEST_CASE("DICEROLL_BASIC_TEST", "[diceword]")
{
    diceware::Diceroll instance(8);
    CHECK(instance.nb_faces() == 8);
}

TEST_CASE("DICEROLL_BOOL_OPERATORS", "[diceword]")
{
    diceware::Diceroll instance(6);
    CHECK(bool(instance) == false);
    CHECK(bool(!instance) == true);
    instance.make_roll();
    CHECK(bool(instance) == true);
    CHECK(bool(!instance) == false);
}
