#ifndef DICEWARE_RANDOM_SOURCE_FACTORY_HPP
#define DICEWARE_RANDOM_SOURCE_FACTORY_HPP

#include "diceware/random_source.hpp"
#include <memory>
#include <string>

namespace diceware {
class RandomSourceFactory {
public:
    std::unique_ptr<RandomSource> get_new_source(std::string ident, RandomSource::random_int_t inclusive_min, RandomSource::random_int_t inclusive_max);
};
}

#endif // DICEWARE_RANDOM_SOURCE_FACTORY_HPP
