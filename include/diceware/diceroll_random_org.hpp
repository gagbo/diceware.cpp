#ifndef DICEWARE_DICEROLL_RANDOM_ORG_HPP
#define DICEWARE_DICEROLL_RANDOM_ORG_HPP

#include "diceware/random_org_provider.hpp"
#include <cstdint>
#include <iostream>
#include <random>
#include <string>

namespace diceware {
// Glorified random generator simulating a single die.
class DicerollRandomOrg {
public:
    // Basic constructor asking for the number of faces.
    DicerollRandomOrg(std::uint16_t nb_faces)
        : _nb_faces(nb_faces)
    {
    }
    // Overridden copy constructor to not touch the uncopiable random_device
    DicerollRandomOrg(const DicerollRandomOrg& other)
        : _nb_faces(other._nb_faces) {};
    // Overridden move constructor to not touch the unmovable random_device
    DicerollRandomOrg(const DicerollRandomOrg&& other)
        : _nb_faces(std::move(other._nb_faces)) {};

    // Populate the "value" of the roll to be used later.
    // Consecutive make_roll() calls change the underlying value.
    void make_roll();

    // Return the current value of the "die"
    std::uint16_t value() const noexcept { return _value; }

    // Return the number of faces of the "die"
    std::uint16_t nb_faces() const noexcept { return _nb_faces; }

    inline explicit operator bool() const noexcept { return _value != 0; }
    inline bool operator!() const noexcept { return _value == 0; }

    // Pretty print
    friend std::ostream& operator<<(std::ostream& os, const DicerollRandomOrg& dr);

protected:
    // Actual source of randomness
    RandomOrgProvider _rd { 1, 6 };

private:
    std::uint16_t _nb_faces { 6 };
    // a value of 0 is always invalid
    std::uint16_t _value { 0 };
};
} // namespace diceware

#endif // DICEWARE_DICEROLL_RANDOM_ORG_HPP
