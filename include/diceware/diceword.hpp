#ifndef DICEWARE_DICEWORD_HPP_
#define DICEWARE_DICEWORD_HPP_

#include "diceroll.hpp"
#include "diceroll_random_org.hpp"
#include <cstdint>
#include <iostream>
#include <string>
#include <vector>

namespace diceware {
// A group of Dicerolls which form a single logical unit (a word in the list, or
// salt)
class Diceword {
public:
    // TODO : Check if using RandomSource::random_int_t would be better
    // More consistency and only 1 point to move to avoid implicit conversions,
    // vs having RandomSource leakin more and more in the code.

    // Holds the number of faces per die
    using faces_t = std::uint16_t;

    // Build a Diceword using this succession of dice.
    // Each value in the vector is the number of faces on the associated die
    Diceword(std::vector<faces_t> dice_nb_faces, std::string random_source_type = "std");

    // Return the number of throws in the Diceword
    size_t nb_throws() const { return _rolls.size(); }

    // Roll all the "dice" included in the Diceword
    // Consecutive calls change the word; the method can be used as a costly flush
    // to prevent peeking.
    void make_rolls();

    // Pretty print
    friend std::ostream& operator<<(std::ostream& os, const Diceword& dw);

    // Read-only access to die values
    faces_t operator[](size_t i) const { return _rolls[i].value(); }

private:
    // Actual dice rolls
    std::vector<Diceroll> _rolls {};
    // The faces per die.
    std::vector<faces_t> _nb_faces_vector {};
    // Random source type
    std::string _random_source_type { "std" };
};
} // namespace diceware

#endif // DICEWARE_DICEWORD_HPP_
