#ifndef DICEWARE_PASSWORD_HPP
#define DICEWARE_PASSWORD_HPP

#include "diceword.hpp"
#include <cstdint>
#include <map>
#include <memory>
#include <string>

namespace diceware {
// Generate passphrases using Dicewords
class Password {
public:
    // Using 32bits because we have a dumb "atoi" approach to keys
    using key_t = std::uint32_t;

    // TODO : Better parameter parsing for this constructor.
    // This constructor is messy, and also the main entry-point for client-code.
    // Not the best combo.

    // Constructs a passphrase generator from the given filename
    Password(std::string dict_filename, std::uint8_t nb_words,
        std::uint8_t nb_throws_per_word, std::uint8_t nb_faces_per_die,
        bool has_salt = true, std::string words_random_source = "std",
        std::string salt_random_source = "std");

    // Yield a new passphrase
    std::string passphrase();
    // Yield a new passphrase without spaces
    std::string passphrase_without_spaces();

    // Update the dictionary to a new words list
    // The new words list has to have the same number of throws per word
    void read_dict_file(std::string dict_filename);

private:
    // Salt character pool. The size of the array is hard coded.
    // It *could* be better to provide setters and dynamic salt, but
    // at this point the most important thing is to provide a good random
    // source for Diceroll anyway. So this is fine.
    std::array<std::array<char, 6>, 6> _salt_char_table {
        { { '~', '!', '#', '$', '%', '^' },
            { '&', '*', '(', ')', '-', '=' },
            { '+', '[', ']', '\\', '{', '}' },
            { ':', ';', '"', '\'', '<', '>' },
            { '?', '/', '0', '1', '2', '3' },
            { '4', '5', '6', '7', '8', '9' } }
    };

    // Actual dictionnary for the mapping between dice rolls and string words
    std::map<key_t, std::string> _dict {};

    bool _has_salt { true };

    // The rolls vector
    // Dicewords do not like being copied, so iterate on references
    // i.e. use for(auto& word : _words) and don't forget the &
    std::vector<Diceword> _words {};
    std::string _words_random_source { "std" };

    // The salt is a unique_ptr because it is optional.
    std::unique_ptr<Diceword> _salt { nullptr };
    std::string _salt_random_source { "std" };

    // Return a char from the salt char table
    char get_salt_char(Diceword::faces_t index_1, Diceword::faces_t index_2);

    // Return a vector of string corresponding to each word in the passphrase
    std::vector<std::string> get_individual_words();

    // Return a vector of strings with the actual component of a passphrase using
    // _salt
    std::vector<std::string> salt_word(std::vector<std::string> words_list);

    // Make all the dice rolls
    void make_rolls();

    // Generate a new passphrase with join_char between each random word.
    // This method uses _has_salt to determine if the passphrase is salted.
    std::string generate_new_passphrase(char join_char);
};
} // namespace diceware

#endif // DICEWARE_PASSWORD_HPP
