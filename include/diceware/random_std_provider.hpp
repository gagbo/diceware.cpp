#ifndef DICEWARE_RANDOM_STD_PROVIDER
#define DICEWARE_RANDOM_STD_PROVIDER

#include "diceware/random_source.hpp"
#include <random>
#include <string>

namespace diceware {
class RandomStdProvider : public RandomSource {
public:
    RandomStdProvider(random_int_t inclusive_min, random_int_t inclusive_max);
    RandomStdProvider(const RandomStdProvider& other)
        : RandomSource(other._min, other._max)
    {
    }
    RandomStdProvider(const RandomStdProvider&& other)
        : RandomSource(other._min, other._max)
    {
    }
    virtual random_int_t yield_int() override;

    static std::string ENTROPY_WARNING;

private:
    std::random_device _rd {};
};
}

#endif // DICEWARE_RANDOM_STD_PROVIDER
