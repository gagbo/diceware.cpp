#ifndef DICEWARE_RANDOM_SOURCE_HPP
#define DICEWARE_RANDOM_SOURCE_HPP

#include <cstdint>

namespace diceware {
class RandomSource {
public:
    using random_int_t = std::uint16_t;
    RandomSource(random_int_t inclusive_min, random_int_t inclusive_max)
        : _min(inclusive_min)
        , _max(inclusive_max)
    {
    }
    virtual ~RandomSource() {}
    virtual random_int_t yield_int() = 0;
    virtual inline random_int_t inclusive_min_value() const { return _min; }
    virtual inline random_int_t inclusive_max_value() const { return _max; }

protected:
    random_int_t _min { 1 };
    random_int_t _max { 6 };
};
}

#endif // DICEWARE_RANDOM_SOURCE_HPP
