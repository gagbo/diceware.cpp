#ifndef DICEWARE_RANDOM_ORG_PROVIDER_HPP
#define DICEWARE_RANDOM_ORG_PROVIDER_HPP

#include "diceware/random_source.hpp"
#include <cstdint>
#include <fstream>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

namespace diceware {
class RandomOrgProvider : public RandomSource {
public:
    static size_t request_count;
    // Default constructor asking for inclusive minimum and maximum values,
    // and optional API key.
    //
    // If the key is not given, and
    // a file runtime/secrets/api_key-random_org file exists,
    // the first line of this file is used as API key
    RandomOrgProvider(random_int_t inclusive_min, random_int_t inclusive_max, std::string api_key = "");

    // Yield a random integer within bounds from the random.org API
    virtual random_int_t yield_int() override;

    inline void set_api_key(std::string api_key) { _api_key = api_key; }

    // Set the cache size within the RandomOrgProvider instance
    //
    // Each call to yield_int does not necessarily call the API. Each API call
    // asks for n random integers, which are stored and then given one by one until
    // there are no more. the "n" is the cache size, and this function is a setter.
    inline void set_cache_size(size_t new_cache_size) { _cache_size = new_cache_size; }

private:
    std::string _api_key {};
    size_t _cache_size { 128 };
    std::vector<random_int_t> _cached_values {};

    void fill_cache();
    // Generate the json to send for a generateIntegers call on Random.org API
    nlohmann::json generateIntegers_call();
};
}

#endif // DICEWARE_RANDOM_ORG_PROVIDER_HPP
