#ifndef DICEWARE_DICEROLL_HPP
#define DICEWARE_DICEROLL_HPP

#include "diceware/random_source_factory.hpp"
#include <cstdint>
#include <iostream>
#include <memory>
#include <random>
#include <string>

namespace diceware {
// Glorified random generator simulating a single die.
class Diceroll {
public:
    // Basic constructor asking for the number of faces.
    Diceroll(std::uint16_t nb_faces, std::string ident = "std")
        : _nb_faces(nb_faces)
        , _factory_arg(ident)
    {
        RandomSourceFactory fact;
        _source = fact.get_new_source(_factory_arg, 1, _nb_faces);
    }
    // Overridden copy constructor for the unique_ptr
    Diceroll(const Diceroll& other)
        : _nb_faces(other._nb_faces)
        , _factory_arg(other._factory_arg)
    {
        RandomSourceFactory fact;
        _source = fact.get_new_source(_factory_arg, 1, _nb_faces);
    }
    // Overridden move constructor for the unique_ptr
    Diceroll(Diceroll&& other)
        : _nb_faces(std::move(other._nb_faces))
        , _factory_arg(std::move(other._factory_arg))
        , _source(std::move(other._source))
    {
    }

    // Populate the "value" of the roll to be used later.
    // Consecutive make_roll() calls change the underlying value.
    void make_roll();

    // Return the current value of the "die"
    RandomSource::random_int_t value() const noexcept { return _value; }

    // Return the number of faces of the "die"
    RandomSource::random_int_t nb_faces() const noexcept { return _nb_faces; }

    inline explicit operator bool() const noexcept { return _value != 0; }
    inline bool operator!() const noexcept { return _value == 0; }

    // Pretty print
    friend std::ostream& operator<<(std::ostream& os, const Diceroll& dr);

protected:
    RandomSource::random_int_t _nb_faces { 6 };
    std::string _factory_arg { "std" };

    std::unique_ptr<RandomSource> _source { nullptr };
    // a value of 0 is always invalid
    RandomSource::random_int_t _value { 0 };
};
} // namespace diceware

#endif // DICEWARE_DICEROLL_HPP
