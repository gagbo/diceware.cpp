#include "diceware/random_org_provider.hpp"

#include <iostream>
#include <restclient-cpp/restclient.h>

namespace diceware {
size_t RandomOrgProvider::request_count = 0;
RandomOrgProvider::RandomOrgProvider(random_int_t inclusive_min, random_int_t inclusive_max, std::string api_key)
    : RandomSource(inclusive_min, inclusive_max)
    , _api_key(std::move(api_key))
{
    if (_api_key == "") {
        std::ifstream input_key("../runtime/secrets/api_key-random_org");
        if (input_key) {
            std::string key;
            std::getline(input_key, key);
            set_api_key(key);
        }
    }
}

RandomOrgProvider::random_int_t RandomOrgProvider::yield_int()
{
    if (_cached_values.size() == 0) {
        fill_cache();
    }
    if (_cached_values.size() == 0) {
        std::cerr << "Could not fill the cache properly\n";
        exit(1);
    }

    auto ret = _cached_values.back();
    _cached_values.pop_back();
    ;

    return ret;
}

void RandomOrgProvider::fill_cache()
{
    _cached_values.clear();
    nlohmann::json request = generateIntegers_call();
    // Get the response from the server
    // Use application/json-rpc
    RestClient::Response result = RestClient::post(
        "https://api.random.org/json-rpc/1/invoke",
        "application/json-rpc",
        request.dump());
    if (static_cast<size_t>(result.code / 100) == 4) {
        std::cerr << "Client side error\n"
                  << result.body << "\n";
        exit(1);
    }
    if (static_cast<size_t>(result.code / 100) == 5) {
        std::cerr << "Server side error\n"
                  << result.body << "\n";
        exit(1);
    }
    nlohmann::json desser_result = nlohmann::json::parse(result.body);
    std::cerr << "You have "
              << desser_result["result"]["bitsLeft"] << " bits left, and "
              << desser_result["result"]["requestsLeft"] << " requests left "
              << "on this API key. All these values reset at 00:00AM UTC.\n";
    _cached_values.reserve(_cache_size);
    for (auto& value : desser_result["result"]["random"]["data"]) {
        _cached_values.push_back(value);
    }
}

nlohmann::json RandomOrgProvider::generateIntegers_call()
{
    nlohmann::json request;
    request["jsonrpc"] = "2.0";
    request["method"] = "generateIntegers";
    request["id"] = request_count;

    // This line means we need to check the id of the response against
    // request_count - 1
    request_count++;

    nlohmann::json params;
    params["apiKey"] = _api_key;
    params["n"] = _cache_size;
    params["min"] = _min;
    params["max"] = _max;
    params["base"] = 10;
    params["replacement"] = true;

    request["params"] = params;
    return request;
}
}
