#include "diceware/random_source_factory.hpp"
#include "diceware/random_org_provider.hpp"
#include "diceware/random_std_provider.hpp"
#include <iostream>

namespace diceware {
std::unique_ptr<RandomSource> RandomSourceFactory::get_new_source(std::string ident,
    RandomSource::random_int_t inclusive_min, RandomSource::random_int_t inclusive_max)
{
    if (ident == "std") {
        return std::make_unique<RandomStdProvider>(inclusive_min, inclusive_max);
    } else if (ident == "random.org") {

        return std::make_unique<RandomOrgProvider>(inclusive_min, inclusive_max);
    } else {
        std::cerr << "Unkwnown identifier in factory : " << ident << "\n";
        exit(1);
    }
}
}
