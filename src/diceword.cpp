#include <diceware/diceword.hpp>

namespace diceware {
  Diceword::Diceword(std::vector<faces_t> dice_nb_faces, std::string random_source_type)
    : _nb_faces_vector(dice_nb_faces), _random_source_type(random_source_type)
{
    for (auto nb_faces : _nb_faces_vector) {
      _rolls.emplace_back(nb_faces, _random_source_type);
    }
}

void Diceword::make_rolls()
{
    for (auto& die : _rolls) {
        die.make_roll();
    }
}

std::ostream& operator<<(std::ostream& os, const diceware::Diceword& dw)
{
    os << "Diceword : ";
    for (auto& roll : dw._rolls) {
        os << roll << " ";
    }
    os << "\n";
    return os;
}
} // namespace diceware
