#include <diceware/password.hpp>
#include <fstream>
#include <sstream>

namespace diceware {
Password::Password(std::string dict_filename, std::uint8_t nb_words,
    std::uint8_t nb_throws_per_word,
    std::uint8_t nb_faces_per_die, bool has_salt,
    std::string words_random_source, std::string salt_random_source)
    : _has_salt(has_salt)
    , _words_random_source(words_random_source)
    , _salt_random_source(salt_random_source)
{
    read_dict_file(dict_filename);

    // Contain the list of faces per die to use to pick a word in the dict
    std::vector<Diceword::faces_t> dummy_const;
    for (size_t i = 0; i < nb_throws_per_word; ++i) {
        dummy_const.push_back(nb_faces_per_die);
    }

    for (size_t i = 0; i < nb_words; ++i) {
        _words.push_back(Diceword(dummy_const, _words_random_source));
    }
}
std::string Password::generate_new_passphrase(char join_char)
{
    make_rolls();
    std::vector<std::string> strings_to_join;
    if (_has_salt) {
        strings_to_join = salt_word(get_individual_words());
    } else {
        strings_to_join = get_individual_words();
    }
    std::stringstream ss;
    for (auto str : strings_to_join) {
        ss << str << join_char;
    }
    auto res = ss.str();
    res.pop_back();
    return res;
}
std::string Password::passphrase() { return generate_new_passphrase(' '); }

std::string Password::passphrase_without_spaces()
{
    return generate_new_passphrase('0');
}

char Password::get_salt_char(Diceword::faces_t index_1,
    Diceword::faces_t index_2)
{
    // indices are dice rolls, so 1-indexed
    return _salt_char_table[static_cast<size_t>(index_1 - 1)]
                           [static_cast<size_t>(index_2 - 1)];
}

std::vector<std::string> Password::get_individual_words()
{
    std::vector<std::string> res;
    for (auto& word : _words) {
        key_t base_10_value = 0;
        for (size_t i = 0; i < word.nb_throws(); ++i) {
            base_10_value += word[i] * static_cast<key_t>(std::pow(10, word.nb_throws() - i - 1));
        }
        res.push_back(_dict[base_10_value]);
    }
    return res;
}

std::vector<std::string> Password::salt_word(std::vector<std::string> before)
{
    // Move the copy so we prevent an additional one
    auto after = std::move(before);
    while ((*_salt)[0] > after.size()) {
        _salt->make_rolls();
    }

    auto salt_char = get_salt_char((*_salt)[2], (*_salt)[3]);
    size_t salted_word_index = static_cast<size_t>((*_salt)[0] - 1);
    size_t salt_position = static_cast<size_t>((*_salt)[1] - 1);
    salt_position = std::min(salt_position, after[salted_word_index].size() - 1);

    after[salted_word_index][salt_position] = salt_char;
    return after;
}

void Password::make_rolls()
{
    for (auto& word : _words) {
        word.make_rolls();
    }

    if (_has_salt) {
        if (!_salt) {
            // C++ 14 _salt = std::make_unique({_words.size(), 6, 6, 6});

            std::vector<Diceword::faces_t> salt_faces;
            salt_faces.reserve(4);
            salt_faces.push_back(static_cast<Diceword::faces_t>(_words.size()));
            salt_faces.push_back(6);
            salt_faces.push_back(6);
            salt_faces.push_back(6);
            _salt = (std::unique_ptr<Diceword>(
                new Diceword(salt_faces, _salt_random_source)));
        }
        _salt->make_rolls();
    }
}

void Password::read_dict_file(std::string dict_filename)
{
    auto input_stream = std::ifstream(dict_filename);
    if (!input_stream) {
        std::cout << "Could not read " << dict_filename << "\n";
        exit(1);
    }

    std::string line;
    while (std::getline(input_stream, line)) {
        std::istringstream iss(line);
        key_t value;
        std::string word_in_list;
        iss >> value >> word_in_list;
        _dict[value] = word_in_list;
    }
}

} // namespace diceware
