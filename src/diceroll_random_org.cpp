#include <diceware/diceroll_random_org.hpp>
#include <iostream>

namespace diceware {
void DicerollRandomOrg::make_roll()
{
    _value = _rd.yield_int();
    return;
}

std::ostream& operator<<(std::ostream& os, const diceware::DicerollRandomOrg& dr)
{
    os << dr._value;
    return os;
}
} // namespace diceware
