#include <diceware/diceroll.hpp>
#include <iostream>

namespace diceware {
void Diceroll::make_roll()
{
    _value = _source->yield_int();
    return;
}

std::ostream& operator<<(std::ostream& os, const diceware::Diceroll& dr)
{
    os << dr._value;
    return os;
}
} // namespace diceware
