#include "diceware/random_std_provider.hpp"
#include <iostream>

std::string diceware::RandomStdProvider::ENTROPY_WARNING = "Warning : the entropy of your random number generator suggests "
                                                           "that your random generator is not a \"true\", cryptographically valid "
                                                           "random generator, "
                                                           "but actually a pseudo-one. You should use another method to generate "
                                                           "passphrases.";
diceware::RandomStdProvider::RandomStdProvider(diceware::RandomSource::random_int_t inclusive_min, diceware::RandomSource::random_int_t inclusive_max)
    : RandomSource(inclusive_min, inclusive_max)
{
}

diceware::RandomSource::random_int_t diceware::RandomStdProvider::yield_int()
{
    std::uniform_int_distribution<diceware::RandomSource::random_int_t> dist(_min, _max);
    if (_rd.entropy() == 0) {
        std::cerr << ENTROPY_WARNING << "\n";
    }
    return dist(_rd);
}
