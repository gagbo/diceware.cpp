#include "diceware/Version.hpp"
#include "diceware/diceword.hpp"
#include "diceware/password.hpp"
#include <iostream>
#include <vector>

int main()
{
    diceware::Diceword instance(
        std::vector<diceware::Diceword::faces_t> { 6, 6, 6, 6, 6 });
    std::cout << "Before making rolls (should be unvalid values) : " << instance
              << "\n";
    instance.make_rolls();
    std::cout << "After rolling : " << instance << "\n";

    diceware::Password pass_fr_5j("../runtime/dicts/diceware-fr-5-jets.txt", 5, 5,
                                  6, true, "random.org", "std");
    std::cout << pass_fr_5j.passphrase() << "\n";
    std::cout << pass_fr_5j.passphrase() << "\n";
    std::cout << pass_fr_5j.passphrase() << "\n";
    std::cout << pass_fr_5j.passphrase() << "\n";

    return 0;
}
